'use strict';

/*
You are given a 3-D Matrix in which each block contains 0 initially. The first block is defined by the coordinate (1,1,1) and the last block is defined by the coordinate (N,N,N). There are two types of queries.*/

//crear matriz de dimension N
function crearMatrix(N){
	var a = [];

	for (var i = 0; i < N; i++) {
		a[i] = [];
		for (var j = 0; j < N; j++) {
			a[i][j] = [];
			for (var k = 0; k < N; k++) {
				a[i][j][k] = 0;
			}
		}
	}
	return a;
}

/*
UPDATE x y z W
updates the value of block (x,y,z) to W.
*/

//actualizar el valor n en la posicion x,y,z de la matriz
function update(x,y,z,n,matriz){
	matriz[z-1][y-1][x-1] = parseInt(n);
}

/*
QUERY x1 y1 z1 x2 y2 z2
calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive), y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive).
*/

//encuentra la suma desde la posicion p1 hasta p2 de la matriz, p1 y p2 es un arreglo de [x,y,z]
function query(valores,matriz){
	var p1 = valores.slice(0,3);
	var p2 = valores.slice(3,6);
	var sum = 0;
	for (var i = p1[2]; i <= p2[2]; i++) {
		for (var j = p1[1]; j <= p2[1]; j++) {
			sum += sumArray(p1[0],p2[0],matriz[i-1][j-1]);
		}
	}
	return sum;
}

//encuentra la suma de un arreglo desde la posicion x hasta y
function sumArray(x,y,arreglo){
	var a = 0;
	for (var i = x - 1; i <= y - 1; i++) {
		a += arreglo[i];
	}
	return a;
}

/*
Input Format 
The first line contains an integer T, the number of test-cases. T testcases follow. 
For each test case, the first line will contain two integers N and M separated by a single space. 
N defines the N * N * N matrix. 
M defines the number of operations. 
The next M lines will contain either

 1. UPDATE x y z W
 2. QUERY  x1 y1 z1 x2 y2 z2 
Output Format 
Print the result for each QUERY.

Constrains 
1 <= T <= 50 
1 <= N <= 100 
1 <= M <= 1000 
1 <= x1 <= x2 <= N 
1 <= y1 <= y2 <= N 
1 <= z1 <= z2 <= N 
1 <= x,y,z <= N 
-10^9 <= W <= 10^9
*/

//functiones para validar los rangos de T, N, M, x, y, z y w
function validT(T){
	if(T <= 50 && T >= 1)
		return true;
	else
		return false
}

function validNM(Nums){
	var N = Nums[0];
	var M = Nums[1];
	if(N <= 100 && N >= 1 && M <= 1000 && M >= 1)
		return true;
	else
		return false;
}

function validQ(nums,N){
	var a = true;
	for (var i = 0; i < nums.length; i++) {
		if((i<3 && (nums[i] > nums[i+3] || nums[i] < 1)) || (i>=3 && (nums[i] < nums[i-3] || nums[i] > N))){
			a = false;
			break;			
		}
	}
	return a;
}

function validU(nums,N){
	var a = true;
	for (var i = 0; i < nums.length-1; i++) {
		if(nums[i] > N || nums[i] < 1){
			a = false;
			break;			
		}
	}
	if(a && (nums[3] > 10**9 || nums[3] < -(10**9)))
		a = false;
	return a;
}

/*
Sample Input

2
4 5
UPDATE 2 2 2 4
QUERY 1 1 1 3 3 3
UPDATE 1 1 1 23
QUERY 2 2 2 4 4 4
QUERY 1 1 1 3 3 3
2 4
UPDATE 2 2 2 1
QUERY 1 1 1 1 1 1
QUERY 1 1 1 2 2 2
QUERY 2 2 2 2 2 2
Sample Output
*/

//agregar texto como parrafo al jumbotron
function addTexto(texto,tipo) {
	if(tipo == 'consola'){
		var outHtml = "<p style='text-align:right'>"+texto+"</p>";
		$('#pregunta').html(outHtml);		
	}
	else if(tipo == 'usuario'){
		var outHtml = "<p style='text-align:right'>"+texto+"</p>";
		$('#consola').html(outHtml + $('#consola').html());			
	}
	
}

//evento enter sobre el input
document.getElementsByTagName('input')[0].addEventListener('keydown', function(event) {
   	if(event.keyCode == 13)
        enviar(this.form);
});

//evitar el submit del formulario
$("#myform").submit(function(e){
	e.preventDefault();
})  


var t = 0;
var n = 0;
var m = 0;
var matriz;

//preguntar al usuario
function preguntar(){
	if(t == 0)
		preguntarT();
	else
		if(m == 0)
			preguntarNM();
		else
			preguntarQuery();
}

function validar(numero){
	if(t == 0)
		if(!isNaN(numero)) {
			if(validT(numero)){
				t = numero;
				preguntar();
			}
			else
				addTexto('Numero fuera de rango','consola');
		}
		else
			addTexto('Numero Invalido','consola');
	else
		if(m == 0){
    		var regular = new RegExp(/^[1-9][0-9]*\s[1-9][0-9]*$/g);
    		if(regular.test(numero)){
    			var nums = numero.split(" ");
    			if(validNM(nums)){
    				n = nums[0];
    				matriz = crearMatrix(n);
    				m = nums[1];
   					preguntar();
    			}
    			else
    				addTexto('Numeros fuera de rango','consola');
    		}
    		else	
    			addTexto('Entrada Invalida','consola');
		}
		else
			if($('select').val() == 'q'){
				var regular = new RegExp(/^[1-9][0-9]*\s[1-9][0-9]*\s[1-9][0-9]*\s[1-9][0-9]*\s[1-9][0-9]*\s[1-9][0-9]*$/g);
	    		if(regular.test(numero)){
	    			var nums = numero.split(" ");
	    			if(validQ(nums,n)){
	    				addTexto(query(nums,matriz),'usuario');
	    				m--;
	    				if(m == 0)
	    					t--;
	    				preguntar();
	    			}
	    			else
	    				addTexto('Valores Invalidos','consola');
	    		}
	    		else	
	    			addTexto('Entrada Invalida','consola');		
			}
			else if($('select').val() == 'u'){
				var regular = new RegExp(/^[1-9][0-9]*\s[1-9][0-9]*\s[1-9][0-9]*\s-?[1-9][0-9]*$/);
	    		if(regular.test(numero)){
	    			var nums = numero.split(" ");
	    			if(validU(nums,n)){
	    				update(nums[0],nums[1],nums[2],nums[3],matriz);
	    				m--;
	    				if(m == 0)
	    					t--;
	    				preguntar();
	    			}
	    			else
	    				addTexto('Valores Invalidos','consola');
	    		}
	    		else	
	    			addTexto('Entrada Invalida','consola');	
	    	}
}

function preguntarT() {
	$('select').val('t');
	$('select').prop('disabled', true);
	addTexto("Escriba el valor de T","consola");
}

function preguntarNM() {
	$('select').val('nm');
	$('select').prop('disabled', true);
	addTexto("Escriba el valor de N y M, separados y en ese orden","consola");
}

function preguntarQuery() {
	if($('select').val() != 'q' && $('select').val() != 'u')
		$('select').val('q');
	$('select').prop('disabled', false);
	addTexto("Escoje Update o Query y escribe", "consola");
}

//envio de informacion por parte del usuario
function enviar(form){
	var a = form.texto.value;	
	var b = a;
	if($('select').val() == 'q')
		b = 'QUERY '+ a;
	else if($('select').val() == 'u')
		b = 'UPDATE '+ a;
	addTexto(b,'usuario');
	validar(a);
	form.texto.value = "";
}

preguntar();
document.getElementsByTagName('input')[0].focus();

/*
4
4
27
0
1
1
Explanation 
First test case, we are given a cube of 4 * 4 * 4 and 5 queries. Initially all the cells (1,1,1) to (4,4,4) are 0. 
UPDATE 2 2 2 4 makes the cell (2,2,2) = 4 
QUERY 1 1 1 3 3 3. As (2,2,2) is updated to 4 and the rest are all 0. The answer to this query is 4. 
UPDATE 1 1 1 23. updates the cell (1,1,1) to 23. QUERY 2 2 2 4 4 4. Only the cell (1,1,1) and (2,2,2) are non-zero and (1,1,1) is not between (2,2,2) and (4,4,4). So, the answer is 4. 
QUERY 1 1 1 3 3 3. 2 cells are non-zero and their sum is 23+4 = 27.
*/